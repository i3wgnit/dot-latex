\RequirePackage{twlbase}
\ProvidesExplClass{twl}{2019-05-10}{1.0}{Custom LaTeX Class}


%% Variables and Constants

\tl_new:N \g__twl_base_class_tl
\tl_new:N \g__twl_page_format_tl
\tl_new:N \g__twl_font_size_tl

\tl_gset:Nn \g__twl_base_class_tl { article }
\tl_gset:Nn \g__twl_page_format_tl { letterpaper }
\tl_gset:Nn \g__twl_font_size_tl { 12pt }

% env

\bool_new:N \g__twl_acronym_bool
\bool_new:N \g__twl_frame_bool
\bool_new:N \g__twl_index_bool
\bool_new:N \g__twl_mnote_bool
\bool_new:N \g__twl_theorem_bool
\bool_new:N \g__twl_thmsection_bool
\bool_new:N \g__twl_vcommentclean_bool

\bool_gset_false:N \g__twl_acronym_bool
\bool_gset_false:N \g__twl_frame_bool
\bool_gset_false:N \g__twl_index_bool
\bool_gset_false:N \g__twl_mnote_bool
\bool_gset_true:N \g__twl_theorem_bool
\bool_gset_true:N \g__twl_thmsection_bool
\bool_gset_false:N \g__twl_vcommentclean_bool

% graph

\bool_new:N \g__twl_algorithm_bool
\bool_new:N \g__twl_asymptote_bool
\bool_new:N \g__twl_asyinclude_bool
\bool_new:N \g__twl_diagrams_bool
\bool_new:N \g__twl_graphviz_bool

\bool_gset_false:N \g__twl_algorithm_bool
\bool_gset_false:N \g__twl_asymptote_bool
\bool_gset_true:N \g__twl_asyinclude_bool
\bool_gset_false:N \g__twl_diagrams_bool
\bool_gset_false:N \g__twl_graphviz_bool

% math

\bool_new:N \g__twl_physics_bool
\bool_new:N \g__twl_siunitx_bool
\bool_new:N \g__twl_kbordermatrix_bool

\bool_gset_true:N \g__twl_physics_bool
\bool_gset_false:N \g__twl_siunitx_bool
\bool_gset_false:N \g__twl_kbordermatrix_bool

% style

\bool_new:N \g__twl_colour_bool
\bool_new:N \g__twl_header_bool
\bool_new:N \g__twl_problem_bool
\bool_new:N \g__twl_problemclean_bool
\bool_new:N \g__twl_section_bool
\bool_new:N \g__twl_inlinetitle_bool

\bool_gset_true:N \g__twl_colour_bool
\bool_gset_false:N \g__twl_header_bool
\bool_gset_false:N \g__twl_problem_bool
\bool_gset_false:N \g__twl_problemclean_bool
\bool_gset_true:N \g__twl_section_bool
\bool_gset_false:N \g__twl_inlinetitle_bool


%% Options

% presets

\DeclareOption { lecture }
{
  \tl_gset:Nn \g__twl_base_class_tl { article }

  \bool_gset_true:N \g__twl_acronym_bool
  \bool_gset_true:N \g__twl_frame_bool
  \bool_gset_true:N \g__twl_index_bool
  \bool_gset_true:N \g__twl_mnote_bool
  \bool_gset_true:N \g__twl_theorem_bool
  \bool_gset_true:N \g__twl_thmsection_bool
  \bool_gset_false:N \g__twl_vcommentclean_bool

  \bool_gset_false:N \g__twl_algorithm_bool
  \bool_gset_false:N \g__twl_asymptote_bool
  % \bool_gset_true:N \g__twl_asyinclude_bool
  \bool_gset_false:N \g__twl_diagrams_bool
  \bool_gset_false:N \g__twl_graphviz_bool

  \bool_gset_true:N \g__twl_physics_bool
  \bool_gset_false:N \g__twl_siunitx_bool

  \bool_gset_true:N \g__twl_colour_bool
  \bool_gset_true:N \g__twl_header_bool
  \bool_gset_false:N \g__twl_problem_bool
  \bool_gset_false:N \g__twl_problemclean_bool
  \bool_gset_true:N \g__twl_section_bool
  \bool_gset_false:N \g__twl_inlinetitle_bool
}

\DeclareOption { assignment }
{
  \tl_gset:Nn \g__twl_base_class_tl { article }

  \bool_gset_false:N \g__twl_acronym_bool
  \bool_gset_false:N \g__twl_frame_bool
  \bool_gset_false:N \g__twl_index_bool
  \bool_gset_false:N \g__twl_mnote_bool
  \bool_gset_true:N \g__twl_theorem_bool
  \bool_gset_true:N \g__twl_thmsection_bool
  \bool_gset_false:N \g__twl_vcommentclean_bool

  \bool_gset_false:N \g__twl_algorithm_bool
  \bool_gset_false:N \g__twl_asymptote_bool
  % \bool_gset_true:N \g__twl_asyinclude_bool
  \bool_gset_false:N \g__twl_diagrams_bool
  \bool_gset_false:N \g__twl_graphviz_bool

  \bool_gset_true:N \g__twl_physics_bool
  \bool_gset_false:N \g__twl_siunitx_bool

  \bool_gset_false:N \g__twl_colour_bool
  \bool_gset_false:N \g__twl_header_bool
  \bool_gset_true:N \g__twl_problem_bool
  \bool_gset_false:N \g__twl_problemclean_bool
  \bool_gset_false:N \g__twl_section_bool
  \bool_gset_false:N \g__twl_inlinetitle_bool
}

% toggles

\DeclareOption { report } { \tl_gset:Nn \g__twl_base_class_tl { report } }
\DeclareOption { article } { \tl_gset:Nn \g__twl_base_class_tl { article } }
\DeclareOption { a4paper } { \tl_gset:Nn \g__twl_page_format_tl { a4paper } }
\DeclareOption { letterpaper } { \tl_gset:Nn \g__twl_page_format_tl { letterpaper } }

\DeclareOption { 10pt } { \tl_gset:Nn \g__twl_font_size_tl { 10pt } }
\DeclareOption { 11pt } { \tl_gset:Nn \g__twl_font_size_tl { 11pt } }
\DeclareOption { 12pt } { \tl_gset:Nn \g__twl_font_size_tl { 12pt } }
\DeclareOption { 14pt } { \tl_gset:Nn \g__twl_font_size_tl { 14pt } }
\DeclareOption { 16pt } { \tl_gset:Nn \g__twl_font_size_tl { 16pt } }

% env

\DeclareOption { acronym } { \bool_gset_true:N \g__twl_acronym_bool }
\DeclareOption { noacronym } { \bool_gset_false:N \g__twl_acronym_bool }

\DeclareOption { frame } { \bool_gset_true:N \g__twl_frame_bool }
\DeclareOption { noframe } { \bool_gset_false:N \g__twl_frame_bool }

\DeclareOption { index } { \bool_gset_true:N \g__twl_index_bool }
\DeclareOption { noindex } { \bool_gset_false:N \g__twl_index_bool }

\DeclareOption { mnote } { \bool_gset_true:N \g__twl_mnote_bool }
\DeclareOption { nomnote } { \bool_gset_false:N \g__twl_mnote_bool }

\DeclareOption { notheorem } { \bool_gset_false:N \g__twl_theorem_bool }
\DeclareOption { theorem } { \bool_gset_true:N \g__twl_theorem_bool }

\DeclareOption { nothmsection } { \bool_gset_false:N \g__twl_thmsection_bool }
\DeclareOption { thmsection } { \bool_gset_true:N \g__twl_thmsection_bool }

\DeclareOption { vcommentclean } { \bool_gset_true:N \g__twl_vcommentclean_bool }
\DeclareOption { novcommentclean } { \bool_gset_false:N \g__twl_vcommentclean_bool }

% graph

\DeclareOption { algorithm } { \bool_gset_true:N \g__twl_algorithm_bool }
\DeclareOption { noalgorithm } { \bool_gset_false:N \g__twl_algorithm_bool }

\DeclareOption { asymptote } { \bool_gset_true:N \g__twl_asymptote_bool }
\DeclareOption { noasymptote } { \bool_gset_false:N \g__twl_asymptote_bool }

\DeclareOption { asyattach } { \bool_gset_false:N \g__twl_asyinclude_bool }
\DeclareOption { asyinclude } { \bool_gset_true:N \g__twl_asyinclude_bool }

\DeclareOption { diagrams } { \bool_gset_true:N \g__twl_diagrams_bool }
\DeclareOption { nodiagrams } { \bool_gset_false:N \g__twl_diagrams_bool }

\DeclareOption { graphviz } { \bool_gset_true:N \g__twl_graphviz_bool }
\DeclareOption { nographviz } { \bool_gset_false:N \g__twl_graphviz_bool }

% math

\DeclareOption { nophysics } { \bool_gset_false:N \g__twl_physics_bool }
\DeclareOption { physics } { \bool_gset_true:N \g__twl_physics_bool }

\DeclareOption { siunitx } { \bool_gset_true:N \g__twl_siunitx_bool }
\DeclareOption { nosiunitx } { \bool_gset_false:N \g__twl_siunitx_bool }

\DeclareOption { kbordermatrix } { \bool_gset_true:N \g__twl_kbordermatrix_bool }
\DeclareOption { nokbordermatrix } { \bool_gset_false:N \g__twl_kbordermatrix_bool }

% style

\DeclareOption { clean }
{
  \bool_gset_true:N \g__twl_problemclean_bool
  \bool_gset_true:N \g__twl_vcommentclean_bool
}
\DeclareOption { noclean }
{
  \bool_gset_false:N \g__twl_problemclean_bool
  \bool_gset_false:N \g__twl_vcommentclean_bool
}
\DeclareOption { nocolor } { \bool_gset_false:N \g__twl_colour_bool }
\DeclareOption { color } { \bool_gset_true:N \g__twl_colour_bool }
\DeclareOption { nocolour } { \bool_gset_false:N \g__twl_colour_bool }
\DeclareOption { colour } { \bool_gset_true:N \g__twl_colour_bool }

\DeclareOption { nofancy }
{
  \bool_gset_false:N \g__twl_header_bool
  \bool_gset_false:N \g__twl_section_bool
}
\DeclareOption { fancy }
{
  \bool_gset_true:N \g__twl_header_bool
  \bool_gset_true:N \g__twl_section_bool
}

\DeclareOption { header } { \bool_gset_true:N \g__twl_header_bool }
\DeclareOption { noheader } { \bool_gset_false:N \g__twl_header_bool }

\DeclareOption { problem } { \bool_gset_true:N \g__twl_problem_bool }
\DeclareOption { noproblem } { \bool_gset_false:N \g__twl_problem_bool }

\DeclareOption { problemclean } { \bool_gset_true:N \g__twl_problemclean_bool }
\DeclareOption { noproblemclean } { \bool_gset_false:N \g__twl_problemclean_bool }

\DeclareOption { nosection } { \bool_gset_false:N \g__twl_section_bool }
\DeclareOption { section } { \bool_gset_true:N \g__twl_section_bool }

\DeclareOption { inlinetitle } { \bool_gset_true:N \g__twl_inlinetitle_bool }
\DeclareOption { noinlinetitle } { \bool_gset_false:N \g__twl_inlinetitle_bool }

% Eat all unused options
\DeclareOption* {}
\ProcessOptions\relax


%% Logic

\PassOptionsToClass
{ \tl_use:N \g__twl_page_format_tl }
{ \tl_use:N \g__twl_base_class_tl }

\declpkg { booktabs }
\declpkg { multirow }
\declpkg { soulutf8 }

\RequirePackage { twlenv }
\RequirePackage { twlgraph }
\RequirePackage { twlmath }
\RequirePackage { twlstyle }

\LoadClass [ \tl_use:N \g__twl_font_size_tl ] { \tl_use:N \g__twl_base_class_tl }

\IncludePackages
